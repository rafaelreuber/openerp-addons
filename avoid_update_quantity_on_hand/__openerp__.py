{
    'name': 'Avoid Update Product Quantity on Hand',
    'version': '0.1',
    'author': 'Rafael Reuber',
    'category': 'Product',
    'website': 'http://vortexit.co',
    'description': '''
     Long description.
    ''',
    'depends': ['stock','product'],
    'data': ['product_view.xml','security/product_security.xml'], # Views, Dados, Wizards
    'demo': [],
    'installable': True,
}
